package com.wujunshen.elasticsearch6.common.repository.height;

import com.wujunshen.elasticsearch6.common.domain.height.Highest;
import org.springframework.data.elasticsearch.repository.ElasticsearchRepository;

/**
 * @author frank woo(吴峻申) <br>
 * email:<a href="mailto:frank_wjs@hotmail.com">frank_wjs@hotmail.com</a> <br>
 * @date 2020/2/7 5:33 下午 <br>
 */
public interface HighestRepository extends ElasticsearchRepository<Highest, Long> {
}
