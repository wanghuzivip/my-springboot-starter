package com.wujunshen.elasticsearch6.common.repository.product;

import com.wujunshen.elasticsearch6.common.domain.product.Spu;
import java.util.List;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.elasticsearch.repository.ElasticsearchRepository;
import org.springframework.stereotype.Repository;

/**
 * @author frank woo(吴峻申) <br>
 * email:<a href="mailto:frank_wjs@hotmail.com">frank_wjs@hotmail.com</a> <br>
 * @date 2020/2/7 5:33 下午 <br>
 */
@Repository
public interface SpuRepository extends ElasticsearchRepository<Spu, Long> {
    /**
     * 分页搜索ids集合中的记录
     *
     * @param ids      ids集合
     * @param pageable 分页信息
     * @return 分页对象
     */
    Page<Spu> findByIdIn(List<Long> ids, Pageable pageable);
    
    /**
     * 分页搜索不在ids集合中的记录
     *
     * @param ids      ids集合
     * @param pageable 分页信息
     * @return 分页对象
     */
    Page<Spu> findByIdNotIn(List<Long> ids, Pageable pageable);
}
