package com.wujunshen.elasticsearch6.common.utils;

import lombok.AccessLevel;
import lombok.NoArgsConstructor;

/**
 * @author frank woo(吴峻申) <br>
 * email:<a href="mailto:frank_wjs@hotmail.com">frank_wjs@hotmail.com</a> <br>
 * @date 2020/3/15 1:44 上午 <br>
 */
@NoArgsConstructor(access = AccessLevel.PRIVATE)
public class Constants {
    public static final String ES_INDEX_TEST = "es_test_index";
    public static final String ES_TYPE_TEST = "es_test_type";
    public static final String TEST_INDEX = "my_index";
    public static final String TEST_TYPE = "my_type";
}
