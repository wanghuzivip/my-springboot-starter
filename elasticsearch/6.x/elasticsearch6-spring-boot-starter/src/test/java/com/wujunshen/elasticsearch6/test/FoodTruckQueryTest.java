package com.wujunshen.elasticsearch6.test;

import static com.wujunshen.elasticsearch6.common.utils.Constants.TEST_INDEX;
import static org.hamcrest.CoreMatchers.is;
import static org.junit.Assert.assertThat;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.wujunshen.elasticsearch6.TestConfiguration;
import com.wujunshen.elasticsearch6.common.domain.foodtruck.FoodTruck;
import com.wujunshen.elasticsearch6.common.domain.foodtruck.Location;
import com.wujunshen.elasticsearch6.common.domain.foodtruck.Point;
import com.wujunshen.elasticsearch6.common.domain.foodtruck.TimeRange;
import com.wujunshen.elasticsearch6.common.repository.foodtruck.FoodTruckRepository;
import com.wujunshen.elasticsearch6.wrapper.QueryPair;
import java.io.IOException;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import javax.annotation.Resource;
import lombok.extern.slf4j.Slf4j;
import org.apache.lucene.search.join.ScoreMode;
import org.assertj.core.util.Lists;
import org.elasticsearch.common.unit.Fuzziness;
import org.elasticsearch.index.query.QueryBuilder;
import org.elasticsearch.index.query.QueryBuilders;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.elasticsearch.core.ElasticsearchTemplate;

/**
 * @author frank woo(吴峻申) <br>
 * email:<a href="mailto:frank_wjs@hotmail.com">frank_wjs@hotmail.com</a> <br>
 * @date 2020/2/7 5:33 下午 <br>
 */
@Slf4j
@SpringBootTest(classes = TestConfiguration.class)
public class FoodTruckQueryTest {
    private static final ObjectMapper OBJECT_MAPPER = new ObjectMapper();
    @Resource
    private ElasticsearchTemplate elasticsearchTemplate;
    @Resource
    private FoodTruckRepository foodTruckRepository;
    private QueryPair queryPair;
    
    @BeforeEach
    public void init() {
        queryPair = new QueryPair();
        // 创建index
        if (!elasticsearchTemplate.indexExists(TEST_INDEX)) {
            elasticsearchTemplate.createIndex(TEST_INDEX);
        }
        
        elasticsearchTemplate.putMapping(FoodTruck.class);
        
        elasticsearchTemplate.refresh(FoodTruck.class);
        
        elasticsearchTemplate.putMapping(Location.class);
        
        elasticsearchTemplate.putMapping(Point.class);
        
        elasticsearchTemplate.refresh(Point.class);
        
        elasticsearchTemplate.putMapping(TimeRange.class);
        
        elasticsearchTemplate.refresh(TimeRange.class);
    }
    
    @AfterEach
    public void clear() {
    }
    
    /**
     * 嵌套查询, 内嵌文档查询
     */
    @Test
    public void testPointNestedQuery() throws IOException {
        // 创建数据
        FoodTruck foodTruck =
                FoodTruck.builder()
                        .id(1L)
                        .description("A very nice truck")
                        .location(
                                Location.builder()
                                        .id(1L)
                                        .address("Cologne City")
                                        .point(new Point(1L, 50.9406645, 6.9599115))
                                        .timeRange(
                                                TimeRange.builder()
                                                        .id(1L)
                                                        .from(createTime(8, 30))
                                                        .to(createTime(12, 30))
                                                        .build())
                                        .build())
                        .build();
        
        foodTruckRepository.save(foodTruck);
        
        // 准备查询
        QueryBuilder queryBuilder =
                QueryBuilders.nestedQuery(
                        "point",
                        QueryBuilders.boolQuery()
                                .must(QueryBuilders.matchQuery("point.lat", 50.9406645))
                                .must(QueryBuilders.rangeQuery("point.lon").lt(36.0000).gt(0.000)),
                        ScoreMode.None);
        
        Iterable<FoodTruck> iterable = foodTruckRepository.search(queryBuilder);
        
        List<FoodTruck> list = Lists.newArrayList(iterable);
        
        log.info("\njson string is:{}，list size is:{}\n", OBJECT_MAPPER.writeValueAsString(list), list.size());
        assertThat(list.size(), is(0));
        
        queryBuilder =
                QueryBuilders.nestedQuery(
                        "location.point",
                        QueryBuilders.boolQuery()
                                .must(QueryBuilders.matchQuery("location.point.lat", 50.9406645))
                                .must(QueryBuilders.rangeQuery("location.point.lon").lt(36.0000).gt(0.000)),
                        ScoreMode.None);
        
        iterable = foodTruckRepository.search(queryBuilder);
        
        list = Lists.newArrayList(iterable);
        
        log.info("\njson string is:{}，list size is:{}\n", OBJECT_MAPPER.writeValueAsString(list), list.size());
        assertThat(list.size(), is(1));
        
        queryBuilder =
                QueryBuilders.nestedQuery(
                        "location",
                        QueryBuilders.boolQuery().must(QueryBuilders.matchQuery("location.address", "City")),
                        ScoreMode.None);
        
        iterable = foodTruckRepository.search(queryBuilder);
        
        list = Lists.newArrayList(iterable);
        
        log.info("\njson string is:{}，list size is:{}\n", OBJECT_MAPPER.writeValueAsString(list), list.size());
        assertThat(list.size(), is(1));
        
        foodTruckRepository.delete(foodTruck);
    }
    
    /**
     * 嵌套查询, 内嵌文档查询
     */
    @Test
    public void testTimeRangerNestedQuery() throws IOException {
        // 创建数据
        FoodTruck foodTruck =
                FoodTruck.builder()
                        .id(2L)
                        .description("A very nice truck")
                        .location(
                                Location.builder()
                                        .id(2L)
                                        .address("Cologne City")
                                        .point(new Point(2L, 50.9406645, 6.9599115))
                                        .timeRange(
                                                TimeRange.builder()
                                                        .id(2L)
                                                        .from(createTime(8, 30))
                                                        .to(createTime(12, 30))
                                                        .build())
                                        .build())
                        .build();
        
        foodTruckRepository.save(foodTruck);
        
        // 准备查询
        QueryBuilder queryBuilder =
                QueryBuilders.nestedQuery(
                        "timeRange",
                        QueryBuilders.boolQuery()
                                .must(QueryBuilders.matchQuery("timeRange.from", createTime(8, 30).getTime()))
                                .must(
                                        QueryBuilders.rangeQuery("timeRange.to")
                                                .lt(createTime(13, 0).getTime())
                                                .gt(createTime(12, 0).getTime())),
                        ScoreMode.None);
        
        Iterable<FoodTruck> iterable = foodTruckRepository.search(queryBuilder);
        
        List<FoodTruck> list = Lists.newArrayList(iterable);
        
        log.info("\njson string is:{}，list size is:{}\n", OBJECT_MAPPER.writeValueAsString(list), list.size());
        assertThat(list.size(), is(0));
        
        queryBuilder =
                QueryBuilders.nestedQuery(
                        "location.timeRange",
                        QueryBuilders.boolQuery()
                                .must(
                                        QueryBuilders.matchQuery(
                                                "location.timeRange.from", createTime(8, 30).getTime()))
                                .must(
                                        QueryBuilders.rangeQuery("location.timeRange.to")
                                                .lt(createTime(13, 0).getTime())
                                                .gt(createTime(12, 0).getTime())),
                        ScoreMode.None);
        
        iterable = foodTruckRepository.search(queryBuilder);
        
        list = Lists.newArrayList(iterable);
        
        log.info("\njson string is:{}，list size is:{}\n", OBJECT_MAPPER.writeValueAsString(list), list.size());
        assertThat(list.size(), is(1));
        
        foodTruckRepository.delete(foodTruck);
    }
    
    @Test
    public void testNoNestedQuery() throws IOException {
        // 创建数据
        FoodTruck foodTruck =
                FoodTruck.builder()
                        .id(3L)
                        .description("A very nice truck")
                        .location(
                                Location.builder()
                                        .id(3L)
                                        .address("Cologne City")
                                        .point(new Point(3L, 50.9406645, 6.9599115))
                                        .timeRange(
                                                TimeRange.builder()
                                                        .id(3L)
                                                        .from(createTime(8, 30))
                                                        .to(createTime(12, 30))
                                                        .build())
                                        .build())
                        .build();
        foodTruckRepository.save(foodTruck);
        
        // 准备查询
        queryPair.setFieldNames(new String[]{"description"});
        queryPair.setContent("truck");
        QueryBuilder queryBuilder =
                QueryBuilders.fuzzyQuery(queryPair.getFieldNames()[0], queryPair.getContent())
                        .fuzziness(Fuzziness.ZERO);
        
        Iterable<FoodTruck> iterable = foodTruckRepository.search(queryBuilder);
        
        List<FoodTruck> list = Lists.newArrayList(iterable);
        
        log.debug(
                "\njson string is:{}，list size is:{}\n", OBJECT_MAPPER.writeValueAsString(list), list.size());
        assertThat(list.size(), is(1));
        
        queryPair.setFieldNames(new String[]{"description"});
        queryPair.setContent("truck1");
        queryBuilder =
                QueryBuilders.fuzzyQuery(queryPair.getFieldNames()[0], queryPair.getContent())
                        .fuzziness(Fuzziness.ZERO);
        
        iterable = foodTruckRepository.search(queryBuilder);
        
        list = Lists.newArrayList(iterable);
        
        log.debug(
                "\njson string is:{}，list size is:{}\n", OBJECT_MAPPER.writeValueAsString(list), list.size());
        assertThat(list.size(), is(0));
        
        foodTruckRepository.delete(foodTruck);
    }
    
    private Date createTime(int hour, int minutes) {
        Calendar cal = Calendar.getInstance();
        
        cal.set(Calendar.HOUR_OF_DAY, hour);
        cal.set(Calendar.MINUTE, minutes);
        cal.set(Calendar.SECOND, 0);
        cal.set(Calendar.MILLISECOND, 0);
        cal.set(Calendar.DATE, 0);
        cal.set(Calendar.MONTH, 0);
        cal.set(Calendar.YEAR, 0);
        
        return cal.getTime();
    }
}
