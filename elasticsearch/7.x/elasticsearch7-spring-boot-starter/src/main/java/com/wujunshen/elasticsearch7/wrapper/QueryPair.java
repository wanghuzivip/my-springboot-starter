package com.wujunshen.elasticsearch7.wrapper;

import lombok.Data;

/**
 * @author frank woo(吴峻申) <br>
 * email:<a href="mailto:frank_wjs@hotmail.com">frank_wjs@hotmail.com</a> <br>
 * @date 2020/2/7 5:33 下午 <br>
 */
@Data
public class QueryPair {
    /**
     * 查询的字段名字(可多个)
     */
    private String[] fieldNames;
    /**
     * 查询内容
     */
    private String content;
}
