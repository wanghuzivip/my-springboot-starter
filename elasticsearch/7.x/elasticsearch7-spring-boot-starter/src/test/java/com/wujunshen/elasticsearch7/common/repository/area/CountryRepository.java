package com.wujunshen.elasticsearch7.common.repository.area;

import com.wujunshen.elasticsearch7.common.domain.area.Country;
import org.springframework.data.elasticsearch.repository.ElasticsearchRepository;

/**
 * @author frank woo(吴峻申) <br> email:<a href="mailto:frank_wjs@hotmail.com">frank_wjs@hotmail.com</a> <br>
 * @date 2020/2/7 5:33 下午 <br>
 **/
public interface CountryRepository extends ElasticsearchRepository<Country, Long> {
}
