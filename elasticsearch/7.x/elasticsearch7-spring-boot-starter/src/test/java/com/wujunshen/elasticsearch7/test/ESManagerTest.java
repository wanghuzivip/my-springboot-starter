package com.wujunshen.elasticsearch7.test;

import static com.wujunshen.elasticsearch7.common.utils.Constants.ES_INDEX_TEST;
import static com.wujunshen.elasticsearch7.common.utils.Constants.TEST_INDEX;
import static org.hamcrest.CoreMatchers.equalTo;
import static org.hamcrest.Matchers.containsInAnyOrder;
import static org.hamcrest.Matchers.hasSize;
import static org.hamcrest.Matchers.notNullValue;
import static org.junit.Assert.assertThat;

import com.wujunshen.elasticsearch7.TestConfiguration;
import java.util.List;
import java.util.Set;
import javax.annotation.Resource;
import lombok.extern.slf4j.Slf4j;
import org.elasticsearch.action.ActionFuture;
import org.elasticsearch.action.admin.indices.stats.IndicesStatsRequest;
import org.elasticsearch.action.admin.indices.stats.IndicesStatsResponse;
import org.elasticsearch.client.transport.TransportClient;
import org.elasticsearch.cluster.node.DiscoveryNode;
import org.junit.jupiter.api.Test;
import org.springframework.boot.test.context.SpringBootTest;

/**
 * @author frank woo(吴峻申) <br>
 * email:<a href="mailto:frank_wjs@hotmail.com">frank_wjs@hotmail.com</a> <br>
 * @date 2020/2/7 5:33 下午 <br>
 */
@Slf4j
@SpringBootTest(classes = TestConfiguration.class)
public class ESManagerTest {
    @Resource
    private TransportClient client;
    
    /**
     * 嵌套查询, 内嵌文档查询
     */
    @Test
    public void testIndicesInfo() {
        ActionFuture<IndicesStatsResponse> isr =
                client.admin().indices().stats(new IndicesStatsRequest().all());
        Set<String> set = isr.actionGet().getIndices().keySet();
        log.info("\nsize is:{}\n", set.size());
        
        assertThat(set, notNullValue());
        assertThat(set, hasSize(equalTo(2)));
        
        for (Object indexName : set) {
            log.info("\nindexName is:{}\n", indexName);
        }
        assertThat(set, containsInAnyOrder(ES_INDEX_TEST, TEST_INDEX));
    }
    
    @Test
    public void testNodesInfo() {
        List<DiscoveryNode> nodes = client.connectedNodes();
        for (DiscoveryNode node : nodes) {
            log.info("\nhostAddress is:{}\n", node.getHostAddress());
        }
    }
}
