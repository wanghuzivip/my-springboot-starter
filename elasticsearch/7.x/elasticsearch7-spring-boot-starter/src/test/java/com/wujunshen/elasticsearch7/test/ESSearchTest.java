package com.wujunshen.elasticsearch7.test;

import static org.hamcrest.Matchers.containsString;
import static org.hamcrest.Matchers.equalTo;
import static org.hamcrest.Matchers.is;
import static org.hamcrest.Matchers.notNullValue;
import static org.junit.Assert.assertThat;

import com.wujunshen.elasticsearch7.TestConfiguration;
import com.wujunshen.elasticsearch7.common.domain.product.Sku;
import com.wujunshen.elasticsearch7.common.domain.product.Spu;
import com.wujunshen.elasticsearch7.common.repository.product.SpuRepository;
import com.wujunshen.elasticsearch7.wrapper.QueryPair;
import java.util.Arrays;
import java.util.List;
import javax.annotation.Resource;
import lombok.extern.slf4j.Slf4j;
import org.assertj.core.util.Lists;
import org.elasticsearch.action.search.SearchResponse;
import org.elasticsearch.common.text.Text;
import org.elasticsearch.index.query.DisMaxQueryBuilder;
import org.elasticsearch.index.query.QueryBuilder;
import org.elasticsearch.index.query.QueryBuilders;
import org.elasticsearch.search.SearchHit;
import org.elasticsearch.search.fetch.subphase.highlight.HighlightBuilder;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.elasticsearch.core.ElasticsearchTemplate;
import org.springframework.data.elasticsearch.core.SearchResultMapper;
import org.springframework.data.elasticsearch.core.aggregation.AggregatedPage;
import org.springframework.data.elasticsearch.core.aggregation.impl.AggregatedPageImpl;
import org.springframework.data.elasticsearch.core.query.NativeSearchQueryBuilder;
import org.springframework.data.elasticsearch.core.query.SearchQuery;

/**
 * @author frank woo(吴峻申) <br>
 * email:<a href="mailto:frank_wjs@hotmail.com">frank_wjs@hotmail.com</a> <br>
 * @date 2020/2/7 5:33 下午 <br>
 */
@Slf4j
@SpringBootTest(classes = TestConfiguration.class)
public class ESSearchTest {
    @Resource
    private ElasticsearchTemplate elasticsearchTemplate;
    @Resource
    private SpuRepository spuRepository;
    private Spu spu1;
    private Spu spu2;
    private Spu spu3;
    private QueryPair queryPair;
    private Pageable pageable;
    
    @BeforeEach
    public void init() {
        spu1 = new Spu();
        spu1.setId(1L);
        spu1.setProductCode("7b28c293-4d06-4893-aad7-e4b6ed72c260");
        spu1.setProductName("android手机");
        spu1.setBrandCode("H-001");
        spu1.setBrandName("华为Nexus");
        spu1.setCategoryCode("C-001");
        spu1.setCategoryName("手机");
        
        Sku sku1 = new Sku();
        sku1.setId(1L);
        sku1.setSkuCode("001");
        sku1.setSkuName("华为Nexus P6");
        sku1.setSkuPrice(4000);
        sku1.setColor("Red");
        
        Sku sku2 = new Sku();
        sku2.setId(2L);
        sku2.setSkuCode("002");
        sku2.setSkuName("华为 P8");
        sku2.setSkuPrice(3000);
        sku2.setColor("Blank");
        
        Sku sku3 = new Sku();
        sku3.setId(3L);
        sku3.setSkuCode("003");
        sku3.setSkuName("华为Nexus P6下一代");
        sku3.setSkuPrice(5000);
        sku3.setColor("White");
        
        spu1.getSkus().add(sku1);
        spu1.getSkus().add(sku2);
        spu1.getSkus().add(sku3);
        
        spu2 = new Spu();
        spu2.setId(2L);
        spu2.setProductCode("AVYmdpQ_cnzgjoSZ6ent");
        spu2.setProductName("运动服装");
        spu2.setBrandCode("YD-001");
        spu2.setBrandName("李宁");
        spu2.setCategoryCode("YDC-001");
        spu2.setCategoryName("服装");
        
        Sku sku21 = new Sku(21L, "YD001", "李宁衣服1", "Green", "2XL", 4000);
        Sku sku22 = new Sku(22L, "YD002", "李宁衣服2", "Green", "L", 3000);
        Sku sku23 = new Sku(23L, "YD003", "李宁衣服3", "Green", "M", 5000);
        
        spu2.getSkus().add(sku21);
        spu2.getSkus().add(sku22);
        spu2.getSkus().add(sku23);
        
        spu3 = new Spu();
        spu3.setId(3L);
        spu3.setProductCode("XYY1234567");
        spu3.setProductName("中华人民共和国");
        spu3.setBrandCode("YD-001");
        spu3.setBrandName("米老鼠");
        spu3.setCategoryCode("YDC-001");
        spu3.setCategoryName("服装");
        
        Sku sku31 = new Sku(31L, "LS001", "老鼠的帽子1", "Red", "L", 4000);
        Sku sku32 = new Sku(32L, "LS002", "老鼠的帽子2", "Yellow", "M", 3000);
        Sku sku33 = new Sku(33L, "LS003", "老鼠的帽子3", "Green", "2XL", 5000);
        
        spu3.getSkus().add(sku31);
        spu3.getSkus().add(sku32);
        spu3.getSkus().add(sku33);
        
        spuRepository.save(spu1);
        spuRepository.save(spu2);
        spuRepository.save(spu3);
        
        queryPair = new QueryPair();
        
        // 构建分页、排序条件
        pageable = PageRequest.of(0, 10);
    }
    
    @AfterEach
    public void clear() {
        spuRepository.deleteAll();
        spu1 = null;
        spu2 = null;
        spu3 = null;
        
        queryPair = null;
        pageable = null;
    }
    
    @Test
    public void query() { // 通过ID查询数据
        List<Long> ids = Arrays.asList(1L, 2L, 3L);
        
        Page<Spu> page = spuRepository.findByIdIn(ids, pageable);
        
        assertThat(page, is(notNullValue()));
        assertThat(page.getTotalElements(), equalTo(3L));
        assertThat(page.getTotalPages(), equalTo(1));
        assertThat(page.getContent().size(), equalTo(3));
        
        assertThat(page.getNumber(), equalTo(0));
        assertThat(page.getSize(), equalTo(10));
        
        ids = Arrays.asList(1L, 2L);
        
        page = spuRepository.findByIdNotIn(ids, pageable);
        
        assertThat(page, is(notNullValue()));
        assertThat(page.getTotalElements(), is(equalTo(1L)));
        assertThat(page.getTotalPages(), equalTo(1));
        assertThat(page.getContent().size(), equalTo(1));
        assertThat(page.getContent().get(0).getId(), is(3L));
        
        assertThat(page.getNumber(), equalTo(0));
        assertThat(page.getSize(), equalTo(10));
    }
    
    /**
     * 空查询
     */
    @Test
    public void matchAllQuery() {
        Page<Spu> page = spuRepository.findAll(pageable);
        
        assertThat(page, is(notNullValue()));
        assertThat(page.getTotalElements(), is(equalTo(3L)));
        assertThat(page.getTotalPages(), equalTo(1));
        assertThat(page.getContent().size(), equalTo(3));
        
        assertThat(page.getContent().get(0), equalTo(spu1));
        assertThat(page.getContent().get(1), equalTo(spu2));
        assertThat(page.getContent().get(2), equalTo(spu3));
        
        assertThat(page.getNumber(), equalTo(0));
        assertThat(page.getSize(), equalTo(10));
    }
    
    @Test
    public void matchQuery() {
        queryPair.setFieldNames(new String[]{"productName"});
        queryPair.setContent("android手机");
        QueryBuilder queryBuilder =
                QueryBuilders.matchQuery(queryPair.getFieldNames()[0], queryPair.getContent());
        
        SearchQuery searchQuery =
                new NativeSearchQueryBuilder().withPageable(pageable).withQuery(queryBuilder).build();
        
        Page<Spu> page = spuRepository.search(searchQuery);
        
        assertThat(page, is(notNullValue()));
        assertThat(page.getTotalElements(), is(equalTo(1L)));
        assertThat(page.getTotalPages(), equalTo(1));
        assertThat(page.getContent().size(), equalTo(1));
        assertThat(page.getContent().get(0), equalTo(spu1));
        
        assertThat(page.getNumber(), equalTo(0));
        assertThat(page.getSize(), equalTo(10));
        
        queryPair.setContent("android");
        queryBuilder = QueryBuilders.matchQuery(queryPair.getFieldNames()[0], queryPair.getContent());
        
        searchQuery =
                new NativeSearchQueryBuilder().withPageable(pageable).withQuery(queryBuilder).build();
        
        page = spuRepository.search(searchQuery);
        assertThat(page, is(notNullValue()));
        assertThat(page.getTotalElements(), is(equalTo(1L)));
        assertThat(page.getTotalPages(), equalTo(1));
        assertThat(page.getContent().size(), equalTo(1));
        assertThat(page.getContent().get(0), equalTo(spu1));
        
        assertThat(page.getNumber(), equalTo(0));
        assertThat(page.getSize(), equalTo(10));
        
        queryPair.setContent("xxx");
        queryBuilder = QueryBuilders.matchQuery(queryPair.getFieldNames()[0], queryPair.getContent());
        
        searchQuery =
                new NativeSearchQueryBuilder().withPageable(pageable).withQuery(queryBuilder).build();
        
        page = spuRepository.search(searchQuery);
        assertThat(page, is(notNullValue()));
        assertThat(page.getTotalElements(), is(equalTo(0L)));
        assertThat(page.getTotalPages(), equalTo(0));
        assertThat(page.getContent().size(), equalTo(0));
        
        assertThat(page.getNumber(), equalTo(0));
        assertThat(page.getSize(), equalTo(10));
    }
    
    @Test
    public void multiMatchQuery() {
        queryPair.setFieldNames(new String[]{"productName"});
        queryPair.setContent("人民共和");
        QueryBuilder queryBuilder =
                QueryBuilders.multiMatchQuery(queryPair.getContent(), queryPair.getFieldNames());
        
        SearchQuery searchQuery =
                new NativeSearchQueryBuilder().withPageable(pageable).withQuery(queryBuilder).build();
        
        Page<Spu> page = spuRepository.search(searchQuery);
        
        assertThat(page, is(notNullValue()));
        assertThat(page.getTotalElements(), is(equalTo(1L)));
        assertThat(page.getTotalPages(), equalTo(1));
        assertThat(page.getContent().size(), equalTo(1));
        assertThat(page.getContent().get(0), equalTo(spu3));
        
        assertThat(page.getNumber(), equalTo(0));
        assertThat(page.getSize(), equalTo(10));
    }
    
    @Test
    public void termQuery() {
        queryPair.setFieldNames(new String[]{"productName"});
        queryPair.setContent("android");
        QueryBuilder queryBuilder =
                QueryBuilders.termQuery(queryPair.getFieldNames()[0], queryPair.getContent());
        
        SearchQuery searchQuery =
                new NativeSearchQueryBuilder().withPageable(pageable).withQuery(queryBuilder).build();
        
        Page<Spu> page = spuRepository.search(searchQuery);
        
        assertThat(page, is(notNullValue()));
        assertThat(page.getTotalElements(), is(equalTo(1L)));
        assertThat(page.getTotalPages(), equalTo(1));
        assertThat(page.getContent().size(), equalTo(1));
        assertThat(page.getContent().get(0), equalTo(spu1));
        
        assertThat(page.getNumber(), equalTo(0));
        assertThat(page.getSize(), equalTo(10));
        
        queryPair.setContent("android手机");
        queryBuilder = QueryBuilders.termQuery(queryPair.getFieldNames()[0], queryPair.getContent());
        
        searchQuery =
                new NativeSearchQueryBuilder().withPageable(pageable).withQuery(queryBuilder).build();
        
        page = spuRepository.search(searchQuery);
        
        assertThat(page, is(notNullValue()));
        assertThat(page.getTotalElements(), is(equalTo(0L)));
        assertThat(page.getTotalPages(), equalTo(0));
        assertThat(page.getContent().size(), equalTo(0));
        
        assertThat(page.getNumber(), equalTo(0));
        assertThat(page.getSize(), equalTo(10));
    }
    
    @Test
    public void fuzzyQuery() {
        queryPair.setFieldNames(new String[]{"brandName"});
        queryPair.setContent("李");
        
        QueryBuilder queryBuilder =
                QueryBuilders.fuzzyQuery(queryPair.getFieldNames()[0], queryPair.getContent());
        
        SearchQuery searchQuery =
                new NativeSearchQueryBuilder().withPageable(pageable).withQuery(queryBuilder).build();
        
        Page<Spu> page = spuRepository.search(searchQuery);
        assertThat(page, is(notNullValue()));
        assertThat(page.getTotalElements(), is(equalTo(1L)));
        assertThat(page.getTotalPages(), equalTo(1));
        assertThat(page.getContent().size(), equalTo(1));
        assertThat(page.getContent().get(0), equalTo(spu2));
        
        assertThat(page.getNumber(), equalTo(0));
        assertThat(page.getSize(), equalTo(10));
        
        queryPair.setContent("李宁");
        queryBuilder = QueryBuilders.fuzzyQuery(queryPair.getFieldNames()[0], queryPair.getContent());
        
        searchQuery =
                new NativeSearchQueryBuilder().withPageable(pageable).withQuery(queryBuilder).build();
        
        page = spuRepository.search(searchQuery);
        assertThat(page, is(notNullValue()));
        assertThat(page.getTotalElements(), is(equalTo(0L)));
        assertThat(page.getTotalPages(), equalTo(0));
        assertThat(page.getContent().size(), equalTo(0));
        
        assertThat(page.getNumber(), equalTo(0));
        assertThat(page.getSize(), equalTo(10));
    }
    
    @Test
    public void matchPhrasePrefixQuery() {
        queryPair.setFieldNames(new String[]{"productName"});
        queryPair.setContent("人民共");
        
        QueryBuilder queryBuilder =
                QueryBuilders.matchPhrasePrefixQuery(queryPair.getFieldNames()[0], queryPair.getContent());
        
        SearchQuery searchQuery =
                new NativeSearchQueryBuilder().withPageable(pageable).withQuery(queryBuilder).build();
        
        Page<Spu> page = spuRepository.search(searchQuery);
        assertThat(page, is(notNullValue()));
        assertThat(page.getTotalElements(), is(equalTo(1L)));
        assertThat(page.getTotalPages(), equalTo(1));
        assertThat(page.getContent().size(), equalTo(1));
        assertThat(page.getContent().get(0), equalTo(spu3));
        
        assertThat(page.getNumber(), equalTo(0));
        assertThat(page.getSize(), equalTo(10));
        
        queryPair.setFieldNames(new String[]{"brandName"});
        queryPair.setContent("鼠");
        queryBuilder =
                QueryBuilders.matchPhrasePrefixQuery(queryPair.getFieldNames()[0], queryPair.getContent());
        
        searchQuery =
                new NativeSearchQueryBuilder().withPageable(pageable).withQuery(queryBuilder).build();
        
        page = spuRepository.search(searchQuery);
        assertThat(page, is(notNullValue()));
        assertThat(page.getTotalElements(), is(equalTo(1L)));
        assertThat(page.getTotalPages(), equalTo(1));
        assertThat(page.getContent().size(), equalTo(1));
        assertThat(page.getContent().get(0), equalTo(spu3));
        
        assertThat(page.getNumber(), equalTo(0));
        assertThat(page.getSize(), equalTo(10));
    }
    
    /**
     * 根据ID做搜索
     */
    @Test
    public void idsQuery() {
        String[] idsArray = new String[]{"1"};
        QueryBuilder queryBuilder = QueryBuilders.idsQuery().addIds(idsArray);
        
        SearchQuery searchQuery =
                new NativeSearchQueryBuilder().withPageable(pageable).withQuery(queryBuilder).build();
        
        Page<Spu> page = spuRepository.search(searchQuery);
        assertThat(page, is(notNullValue()));
        assertThat(page.getTotalElements(), is(equalTo(1L)));
        assertThat(page.getTotalPages(), equalTo(1));
        assertThat(page.getContent().size(), equalTo(1));
        assertThat(page.getContent().get(0), equalTo(spu1));
        
        assertThat(page.getNumber(), equalTo(0));
        assertThat(page.getSize(), equalTo(10));
        
        idsArray = new String[]{"1", "2", "3"};
        queryBuilder = QueryBuilders.idsQuery().addIds(idsArray);
        
        searchQuery =
                new NativeSearchQueryBuilder().withPageable(pageable).withQuery(queryBuilder).build();
        
        page = spuRepository.search(searchQuery);
        assertThat(page, is(notNullValue()));
        assertThat(page.getTotalElements(), is(equalTo(3L)));
        assertThat(page.getTotalPages(), equalTo(1));
        assertThat(page.getContent().size(), equalTo(3));
        assertThat(page.getContent().get(0), equalTo(spu1));
        assertThat(page.getContent().get(1), equalTo(spu2));
        assertThat(page.getContent().get(2), equalTo(spu3));
        
        assertThat(page.getNumber(), equalTo(0));
        assertThat(page.getSize(), equalTo(10));
    }
    
    @Test
    public void rangeQuery() {
        queryPair.setFieldNames(new String[]{"productName"});
        QueryBuilder queryBuilder =
                QueryBuilders.rangeQuery(queryPair.getFieldNames()[0])
                        .from("android")
                        .to("服装")
                        .includeLower(true) // 包含上界
                        .includeUpper(true); // 包含下界
        
        SearchQuery searchQuery =
                new NativeSearchQueryBuilder().withPageable(pageable).withQuery(queryBuilder).build();
        
        Page<Spu> page = spuRepository.search(searchQuery);
        
        assertThat(page, is(notNullValue()));
        assertThat(page.getTotalElements(), is(equalTo(3L)));
        assertThat(page.getTotalPages(), equalTo(1));
        assertThat(page.getContent().size(), equalTo(3));
        assertThat(page.getContent().get(0), equalTo(spu1));
        assertThat(page.getContent().get(1), equalTo(spu2));
        assertThat(page.getContent().get(2), equalTo(spu3));
        
        assertThat(page.getNumber(), equalTo(0));
        assertThat(page.getSize(), equalTo(10));
    }
    
    @Test
    public void wildcardQuery() {
        // 避免*开始, 会检索大量内容造成效率缓慢,这里只是示例
        queryPair.setFieldNames(new String[]{"productName"});
        queryPair.setContent("*");
        
        QueryBuilder queryBuilder =
                QueryBuilders.wildcardQuery(queryPair.getFieldNames()[0], queryPair.getContent());
        
        SearchQuery searchQuery =
                new NativeSearchQueryBuilder().withPageable(pageable).withQuery(queryBuilder).build();
        
        Page<Spu> page = spuRepository.search(searchQuery);
        
        assertThat(page, is(notNullValue()));
        assertThat(page.getTotalElements(), is(equalTo(3L)));
        assertThat(page.getTotalPages(), equalTo(1));
        assertThat(page.getContent().size(), equalTo(3));
        assertThat(page.getContent().get(0), equalTo(spu1));
        assertThat(page.getContent().get(1), equalTo(spu2));
        assertThat(page.getContent().get(2), equalTo(spu3));
        
        assertThat(page.getNumber(), equalTo(0));
        assertThat(page.getSize(), equalTo(10));
        
        queryPair.setContent("an*d");
        queryBuilder =
                QueryBuilders.wildcardQuery(queryPair.getFieldNames()[0], queryPair.getContent());
        
        searchQuery =
                new NativeSearchQueryBuilder().withPageable(pageable).withQuery(queryBuilder).build();
        
        page = spuRepository.search(searchQuery);
        
        assertThat(page, is(notNullValue()));
        assertThat(page.getTotalElements(), is(equalTo(1L)));
        assertThat(page.getTotalPages(), equalTo(1));
        assertThat(page.getContent().size(), equalTo(1));
        assertThat(page.getContent().get(0), equalTo(spu1));
        
        assertThat(page.getNumber(), equalTo(0));
        assertThat(page.getSize(), equalTo(10));
    }
    
    @Test
    public void constantScoreQuery() {
        queryPair.setFieldNames(new String[]{"productName"});
        queryPair.setContent("android");
        // 包裹查询, 高于设定分数, 不计算相关性
        QueryBuilder queryBuilder =
                QueryBuilders.constantScoreQuery(
                        QueryBuilders.termQuery(queryPair.getFieldNames()[0], queryPair.getContent()))
                        .boost(2.0f);
        
        SearchQuery searchQuery =
                new NativeSearchQueryBuilder().withPageable(pageable).withQuery(queryBuilder).build();
        
        Page<Spu> page = spuRepository.search(searchQuery);
        
        assertThat(page, is(notNullValue()));
        assertThat(page.getTotalElements(), is(equalTo(1L)));
        assertThat(page.getTotalPages(), equalTo(1));
        assertThat(page.getContent().size(), equalTo(1));
        assertThat(page.getContent().get(0), equalTo(spu1));
        
        assertThat(page.getNumber(), equalTo(0));
        assertThat(page.getSize(), equalTo(10));
    }
    
    @Test
    public void disMaxQuery() {
        QueryBuilder qb1 = QueryBuilders.termQuery("productName", "android");
        QueryBuilder qb2 = QueryBuilders.termQuery("brandName", "李宁");
        
        DisMaxQueryBuilder queryBuilder = QueryBuilders.disMaxQuery();
        queryBuilder.add(qb1);
        queryBuilder.add(qb2);
        queryBuilder.boost(1.3f).tieBreaker(0.7f);
        
        SearchQuery searchQuery =
                new NativeSearchQueryBuilder().withPageable(pageable).withQuery(queryBuilder).build();
        
        Page<Spu> page = spuRepository.search(searchQuery);
        assertThat(page, is(notNullValue()));
        assertThat(page.getTotalElements(), is(equalTo(1L)));
        assertThat(page.getTotalPages(), equalTo(1));
        assertThat(page.getContent().size(), equalTo(1));
        assertThat(page.getContent().get(0), equalTo(spu1));
        
        assertThat(page.getNumber(), equalTo(0));
        assertThat(page.getSize(), equalTo(10));
    }
    
    @Test
    public void queryStringQuery() {
        QueryBuilder queryBuilder = QueryBuilders.queryStringQuery("+android");
        
        SearchQuery searchQuery =
                new NativeSearchQueryBuilder().withPageable(pageable).withQuery(queryBuilder).build();
        
        Page<Spu> page = spuRepository.search(searchQuery);
        assertThat(page, is(notNullValue()));
        assertThat(page.getTotalElements(), is(equalTo(1L)));
        assertThat(page.getTotalPages(), equalTo(1));
        assertThat(page.getContent().size(), equalTo(1));
        assertThat(page.getContent().get(0), equalTo(spu1));
        
        assertThat(page.getNumber(), equalTo(0));
        assertThat(page.getSize(), equalTo(10));
    }
    
    @Test
    public void spanQuery() {
        queryPair.setFieldNames(new String[]{"productName"});
        queryPair.setContent("android");
        QueryBuilder queryBuilder =
                QueryBuilders.spanFirstQuery(
                        QueryBuilders.spanTermQuery(queryPair.getFieldNames()[0], queryPair.getContent()),
                        30000); // Max查询范围的结束位置
        
        SearchQuery searchQuery =
                new NativeSearchQueryBuilder().withPageable(pageable).withQuery(queryBuilder).build();
        
        Page<Spu> page = spuRepository.search(searchQuery);
        assertThat(page, is(notNullValue()));
        assertThat(page.getTotalElements(), is(equalTo(1L)));
        assertThat(page.getTotalPages(), equalTo(1));
        assertThat(page.getContent().size(), equalTo(1));
        assertThat(page.getContent().get(0), equalTo(spu1));
        
        assertThat(page.getNumber(), equalTo(0));
        assertThat(page.getSize(), equalTo(10));
        
        // Span Term
        queryBuilder =
                QueryBuilders.spanTermQuery(queryPair.getFieldNames()[0], queryPair.getContent());
        
        searchQuery =
                new NativeSearchQueryBuilder().withPageable(pageable).withQuery(queryBuilder).build();
        
        page = spuRepository.search(searchQuery);
        
        assertThat(page, is(notNullValue()));
        assertThat(page.getTotalElements(), is(equalTo(1L)));
        assertThat(page.getTotalPages(), equalTo(1));
        assertThat(page.getContent().size(), equalTo(1));
        assertThat(page.getContent().get(0), equalTo(spu1));
        
        assertThat(page.getNumber(), equalTo(0));
        assertThat(page.getSize(), equalTo(10));
    }
    
    @Test
    public void boolQuery() {
        queryPair.setFieldNames(new String[]{"productName"});
        queryPair.setContent("android");
        QueryBuilder queryBuilder =
                QueryBuilders.boolQuery()
                        .must(QueryBuilders.termQuery(queryPair.getFieldNames()[0], queryPair.getContent()));
        
        SearchQuery searchQuery =
                new NativeSearchQueryBuilder().withPageable(pageable).withQuery(queryBuilder).build();
        
        Page<Spu> page = spuRepository.search(searchQuery);
        assertThat(page, is(notNullValue()));
        assertThat(page.getTotalElements(), is(equalTo(1L)));
        assertThat(page.getTotalPages(), equalTo(1));
        assertThat(page.getContent().size(), equalTo(1));
        assertThat(page.getContent().get(0), equalTo(spu1));
        
        assertThat(page.getNumber(), equalTo(0));
        assertThat(page.getSize(), equalTo(10));
    }
    
    @Test
    public void scrollQuery() {
        QueryBuilder queryBuilder = QueryBuilders.matchAllQuery();
        
        SearchQuery searchQuery =
                new NativeSearchQueryBuilder().withPageable(pageable).withQuery(queryBuilder).build();
        
        Page<Spu> page = spuRepository.search(searchQuery);
        
        assertThat(page, is(notNullValue()));
        assertThat(page.getTotalElements(), is(equalTo(3L)));
        assertThat(page.getTotalPages(), equalTo(1));
        assertThat(page.getContent().size(), equalTo(3));
        
        assertThat(page.getNumber(), equalTo(0));
        assertThat(page.getSize(), equalTo(10));
    }
    
    /**
     * 对结果设置高亮显示
     */
    @Test
    public void highLightResultSet() {
        queryPair.setFieldNames(new String[]{"productName"});
        queryPair.setContent("*");
        
        final List<HighlightBuilder.Field> message =
                new HighlightBuilder().field("productName").fields();
        SearchQuery searchQuery =
                new NativeSearchQueryBuilder()
                        .withPageable(pageable)
                        .withQuery(
                                QueryBuilders.wildcardQuery(queryPair.getFieldNames()[0], queryPair.getContent()))
                        .withHighlightFields(message.toArray(new HighlightBuilder.Field[0]))
                        .build();
        
        AggregatedPage<Spu> page =
                elasticsearchTemplate.queryForPage(
                        searchQuery,
                        Spu.class,
                        new SearchResultMapper() {
                            @Override
                            public <T> AggregatedPage<T> mapResults(
                                    SearchResponse response, Class<T> clazz, Pageable pageable) {
                                List<Spu> chunk = Lists.newArrayList();
                                for (SearchHit searchHit : response.getHits()) {
                                    if (response.getHits().getHits().length <= 0) {
                                        return null;
                                    }
                                    Spu spu = new Spu();
                                    spu.setId(Long.valueOf(searchHit.getId()));
                                    
                                    StringBuilder stringBuilder = new StringBuilder();
                                    Text[] text = searchHit.getHighlightFields().get("productName").getFragments();
                                    
                                    if (text != null) {
                                        for (Text str : text) {
                                            stringBuilder.append(str.string());
                                        }
                                        // 遍历 高亮结果集
                                        log.info("遍历 高亮结果集{}", stringBuilder.toString());
                                        spu.setHighlightedMessage(stringBuilder.toString());
                                    }
                                    chunk.add(spu);
                                }
                                if (!chunk.isEmpty()) {
                                    return new AggregatedPageImpl<>((List<T>) chunk, pageable, chunk.size());
                                }
                                return null;
                            }
                            
                            @Override
                            public <T> T mapSearchHit(SearchHit searchHit, Class<T> aClass) {
                                return null;
                            }
                        });
        
        assertThat(page, is(notNullValue()));
        assertThat(page.getTotalElements(), is(equalTo(3L)));
        assertThat(page.getTotalPages(), equalTo(1));
        assertThat(page.getContent().size(), equalTo(3));
        
        assertThat(page.getNumber(), equalTo(0));
        assertThat(page.getSize(), equalTo(10));
        
        assertThat(page.getContent().get(0).getHighlightedMessage(), containsString("android"));
        assertThat((page.getContent().get(1).getHighlightedMessage()), containsString("运"));
        assertThat(page.getContent().get(2).getHighlightedMessage(), containsString("中"));
    }
}
