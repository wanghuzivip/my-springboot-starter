package com.wujunshen.mongodb.entity;

import lombok.Data;

/**
 * @author frank woo(吴峻申) <br>
 * email:<a href="mailto:frank_wjs@hotmail.com">frank_wjs@hotmail.com</a> <br>
 * @date 2020/2/7 1:36 上午 <br>
 */
@Data
public class BaseMongoObject {
    private String id;
}
