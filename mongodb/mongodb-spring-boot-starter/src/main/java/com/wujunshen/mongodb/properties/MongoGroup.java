package com.wujunshen.mongodb.properties;

import lombok.Data;

/**
 * @author frank woo(吴峻申) <br>
 * email:<a href="mailto:frank_wjs@hotmail.com">frank_wjs@hotmail.com</a> <br>
 * @date 2020/2/7 5:33 下午 <br>
 */
@Data
public class MongoGroup {
    private String mongoBeanName = "mongoTemplate";
    private String gridFsBeanName = "gridFsTemplate";
    
    private String mongoTemplateName;
    private String gridFsTemplateName;
    /**
     * 存储时是否保存_class
     */
    private boolean showClass = true;
    /**
     * 基础连接参数
     */
    private String host;
    
    private Integer port = 27017;
    private String database;
    
    /**
     * 每个host的最小连接数
     */
    private int minConnectionsPerHost;
    
    /**
     * 每个host最大连接数
     */
    private int maxConnectionsPerHost = 100;
    
    /**
     * 计算允许多少个线程阻塞等待时的乘数，算法：threadsAllowedToBlockForConnectionMultiplier*connectionsPerHost
     */
    private int threadsAllowedToBlockForConnectionMultiplier = 5;
    
    private int serverSelectionTimeout = 1000 * 30;
    
    /**
     * 连接池获取链接等待时间
     */
    private int maxWaitTime = 1000 * 60 * 2;
    
    /**
     * 连接闲置时间
     */
    private int maxConnectionIdleTime;
    
    /**
     * 连接最多可以使用多久
     */
    private int maxConnectionLifeTime;
    
    /**
     * socket连接超时时间
     */
    private int connectTimeout = 1000 * 10;
    
    /**
     * socket读取超时时间
     */
    private int socketTimeout = 0;
    
    private boolean socketKeepAlive = false;
    private boolean sslEnabled = false;
    private boolean sslInvalidHostNameAllowed = false;
    private boolean alwaysUseMBeans = false;
    
    /**
     * 心跳检测发送频率
     */
    private int heartbeatFrequency = 10000;
    
    /**
     * 最小的心跳检测发送频率
     */
    private int minHeartbeatFrequency = 500;
    
    /**
     * 心跳检测连接超时时间
     */
    private int heartbeatConnectTimeout = 20000;
    
    /**
     * 心跳检测读取超时时间
     */
    private int heartbeatSocketTimeout = 20000;
    
    private int localThreshold = 15;
}
