package com.wujunshen.mongodb;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

/**
 * @author frank woo(吴峻申) <br>
 */
@SpringBootApplication
public class SampleMongodbSpringBootStarterApplication {
    public static void main(String[] args) {
        SpringApplication.run(SampleMongodbSpringBootStarterApplication.class, args);
    }
}