package com.wujunshen.mongodb.entity;

import com.wujunshen.mongodb.annotation.Collection;
import lombok.Builder;
import lombok.Data;
import lombok.EqualsAndHashCode;

/**
 * @author frank woo(吴峻申) <br>
 * Date:  2018/7/23 <br>
 * Time:  14:08 <br>
 * Email: frank_wjs@hotmail.com <br>
 */

@Data
@Builder
@Collection("user")
@EqualsAndHashCode(callSuper = true)
public class User extends BaseMongoObject {
    private String userName;
    
    private String password;
}