package com.wujunshen.mongodb.service.impl;

import com.wujunshen.mongodb.entity.User;
import com.wujunshen.mongodb.service.UserService;
import javax.annotation.Resource;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.stereotype.Service;

/**
 * @author frank woo(吴峻申) <br>
 * Date: 2018/7/24 <br>
 * Time: 18:02 <br>
 * Email: frank_wjs@hotmail.com <br>
 */
@Service
public class UserServiceImpl implements UserService {
    @Resource
    @Qualifier("masterMongoTemplate")
    private MongoTemplate masterMongoTemplate;
    
    @Resource
    @Qualifier("slaveMongoTemplate")
    private MongoTemplate slaveMongoTemplate;
    
    @Override
    public User getUserOfMaster(String userName, String password) {
        Query query = Query.query(Criteria.where("userName").is(userName).and("password").is(password));
        return masterMongoTemplate.findOne(query, User.class);
    }
    
    @Override
    public User getUserOfSlave(String userName, String password) {
        Query query = Query.query(Criteria.where("userName").is(userName).and("password").is(password));
        return slaveMongoTemplate.findOne(query, User.class);
    }
    
    @Override
    public User saveOfMaster(User user) {
        return masterMongoTemplate.save(user);
    }
    
    @Override
    public User saveOfSlave(User user) {
        return slaveMongoTemplate.save(user);
    }
    
    @Override
    public boolean existsOfMaster(String userName) {
        Query query = Query.query(Criteria.where("userName").is(userName));
        return masterMongoTemplate.exists(query, User.class);
    }
    
    @Override
    public boolean existsOfSlave(String userName) {
        Query query = Query.query(Criteria.where("userName").is(userName));
        return slaveMongoTemplate.exists(query, User.class);
    }
}
