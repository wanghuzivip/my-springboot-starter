package com.wujunshen.orika.convert;

import ma.glasnost.orika.MappingContext;
import ma.glasnost.orika.converter.BidirectionalConverter;
import ma.glasnost.orika.metadata.Type;

/**
 * @author frank woo(吴峻申) <br>
 * email:<a href="mailto:frank_wjs@hotmail.com">frank_wjs@hotmail.com</a> <br>
 * @date 2020/2/7 5:33 下午 <br>
 */
public class EnumConvert<T extends Enum<T>> extends BidirectionalConverter<T, Integer> {
    @Override
    public Integer convertTo(T source, Type<Integer> destinationType, MappingContext mappingContext) {
        return source.ordinal() + 1;
    }
    
    @Override
    public T convertFrom(Integer source, Type<T> destinationType, MappingContext mappingContext) {
        return destinationType.getRawType().getEnumConstants()[source - 1];
    }
}
