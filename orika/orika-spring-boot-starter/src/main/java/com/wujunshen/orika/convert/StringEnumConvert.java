package com.wujunshen.orika.convert;

import lombok.extern.slf4j.Slf4j;
import ma.glasnost.orika.MappingContext;
import ma.glasnost.orika.converter.BidirectionalConverter;
import ma.glasnost.orika.metadata.Type;

/**
 * @author frank woo(吴峻申) <br>
 * email:<a href="mailto:frank_wjs@hotmail.com">frank_wjs@hotmail.com</a> <br>
 * @date 2020/2/7 5:33 下午 <br>
 */
@Slf4j
public class StringEnumConvert<T extends Enum<T>> extends BidirectionalConverter<T, String> {
    @Override
    public String convertTo(T source, Type<String> destinationType, MappingContext mappingContext) {
        return source.name();
    }
    
    @Override
    public T convertFrom(String source, Type<T> destinationType, MappingContext mappingContext) {
        for (T t : destinationType.getRawType().getEnumConstants()) {
            if (t.name().contentEquals(source)) {
                return t;
            }
        }
        return null;
    }
}
