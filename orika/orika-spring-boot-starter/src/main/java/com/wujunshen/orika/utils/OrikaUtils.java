package com.wujunshen.orika.utils;

import com.wujunshen.orika.convert.EnumConvert;
import com.wujunshen.orika.convert.JsonConvert;
import com.wujunshen.orika.convert.StringEnumConvert;
import com.wujunshen.orika.convert.ZeroEnumConvert;
import lombok.AccessLevel;
import lombok.NoArgsConstructor;
import ma.glasnost.orika.MapperFactory;

/**
 * @author frank woo(吴峻申) <br>
 * email:<a href="mailto:frank_wjs@hotmail.com">frank_wjs@hotmail.com</a> <br>
 * @date 2020/2/7 5:33 下午 <br>
 */
@NoArgsConstructor(access = AccessLevel.PRIVATE)
public class OrikaUtils {
    public static <D extends Enum<D>> void registerZeroEnumConvert(
            MapperFactory mapperFactory, String convertName) {
        mapperFactory.getConverterFactory().registerConverter(convertName, new ZeroEnumConvert<D>());
    }
    
    public static <D extends Enum<D>> void registerStringEnumConvert(
            MapperFactory mapperFactory, String convertName) {
        mapperFactory.getConverterFactory().registerConverter(convertName, new StringEnumConvert<D>());
    }
    
    public static <D extends Enum<D>> void createEnumConvert(
            MapperFactory mapperFactory, String convertName) {
        mapperFactory.getConverterFactory().registerConverter(convertName, new EnumConvert<D>());
    }
    
    public static <D> void createJsonConvert(MapperFactory mapperFactory, String convertName) {
        mapperFactory.getConverterFactory().registerConverter(convertName, new JsonConvert<D>());
    }
}
