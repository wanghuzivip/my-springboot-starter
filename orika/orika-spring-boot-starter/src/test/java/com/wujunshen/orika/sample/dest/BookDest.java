package com.wujunshen.orika.sample.dest;

import lombok.AllArgsConstructor;
import lombok.Data;

/**
 * @author frank woo(吴峻申) <br>
 * email:<a href="mailto:frank_wjs@hotmail.com">frank_wjs@hotmail.com</a> <br>
 * @date 2020/2/7 5:33 下午 <br>
 */
@Data
@AllArgsConstructor
public class BookDest {
    /**
     * 一个枚举类型
     */
    private BookType bookType;
    
    /**
     * 一个类包含 ISBN 和 page
     */
    private BookInfo bookInfo;
}
