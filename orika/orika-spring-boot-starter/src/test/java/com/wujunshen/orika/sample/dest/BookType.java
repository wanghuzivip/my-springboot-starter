package com.wujunshen.orika.sample.dest;

import lombok.AllArgsConstructor;
import lombok.Getter;

/**
 * @author frank woo(吴峻申) <br>
 * email:<a href="mailto:frank_wjs@hotmail.com">frank_wjs@hotmail.com</a> <br>
 * @date 2020/2/7 5:33 下午 <br>
 */
@Getter
@AllArgsConstructor
public enum BookType {
    科幻(1),
    推理(2),
    历史(3),
    人文(4),
    旅游(5),
    IT(6),
    经管(7),
    漫画(8),
    言情(9),
    武侠(10);
    
    private int value;
    
    public static BookType get(int value) {
        switch (value) {
            case 1:
                return 科幻;
            case 2:
                return 推理;
            case 3:
                return 历史;
            case 4:
                return 人文;
            case 5:
                return 旅游;
            case 6:
                return IT;
            case 7:
                return 经管;
            case 8:
                return 漫画;
            case 9:
                return 言情;
            case 10:
                return 武侠;
            default:
                return 科幻;
        }
    }
}
