package com.wujunshen.orika.sample.dest;

import lombok.AllArgsConstructor;
import lombok.Getter;

/**
 * @author frank woo(吴峻申) <br>
 * email:<a href="mailto:frank_wjs@hotmail.com">frank_wjs@hotmail.com</a> <br>
 * @date 2020/2/7 5:33 下午 <br>
 */
@AllArgsConstructor
@Getter
public enum Leaf {
    NO(0, "否"),
    YES(1, "是");
    
    private Integer key;
    private String value;
}
