package com.wujunshen.orika.sample.dest;

import lombok.AllArgsConstructor;
import lombok.Getter;

/**
 * @author frank woo(吴峻申) <br>
 * email:<a href="mailto:frank_wjs@hotmail.com">frank_wjs@hotmail.com</a> <br>
 * @date 2020/2/7 5:33 下午 <br>
 */
@AllArgsConstructor
@Getter
public enum Sex {
    MALE("M", "男性"),
    FEMALE("F", "女性");
    
    private String key;
    private String value;
}
