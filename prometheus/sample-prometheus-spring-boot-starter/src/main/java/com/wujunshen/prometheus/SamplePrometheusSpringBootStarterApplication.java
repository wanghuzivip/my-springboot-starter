package com.wujunshen.prometheus;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

/**
 * @author frank woo(吴峻申) <br>
 * email:<a href="mailto:frank_wjs@hotmail.com">frank_wjs@hotmail.com</a> <br>
 * @date 2020/2/7 5:33 下午 <br>
 */
@SpringBootApplication
public class SamplePrometheusSpringBootStarterApplication {
    public static void main(String[] args) {
        SpringApplication.run(SamplePrometheusSpringBootStarterApplication.class, args);
    }
}
