package com.wujunshen.prometheus.controller;

import io.micrometer.core.instrument.Counter;
import io.micrometer.core.instrument.MeterRegistry;
import javax.annotation.PostConstruct;
import javax.annotation.Resource;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * @author frank woo(吴峻申) <br>
 * email:<a href="mailto:frank_wjs@hotmail.com">frank_wjs@hotmail.com</a> <br>
 * @date 2020/2/6 7:31 上午 <br>
 */
@RestController
public class CounterController {
    @Resource
    MeterRegistry registry;
    
    private Counter counter;
    
    @PostConstruct
    private void init() {
        counter = registry.counter("app_requests_method_count", "method", "CounterController.counter");
    }
    
    @GetMapping(value = "/counter")
    public Object counter() {
        try {
            counter.increment();
        } catch (Exception e) {
            return e;
        }
        return counter.count() + " monitored by prometheus";
    }
}
