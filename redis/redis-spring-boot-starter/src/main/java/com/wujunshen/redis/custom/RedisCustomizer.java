package com.wujunshen.redis.custom;

import java.util.Map;
import org.springframework.data.redis.cache.RedisCacheConfiguration;

/**
 * 自定义redis缓存配置
 *
 * @author frank woo(吴峻申) <br>
 * email:<a href="mailto:frank_wjs@hotmail.com">frank_wjs@hotmail.com</a> <br>
 * @date 2020/2/7 5:33 下午 <br>
 */
public interface RedisCustomizer {
    /**
     * 自定义redis缓存配置
     *
     * @return 返回map, key为缓存名子, value为redis缓存设置
     */
    Map<String, RedisCacheConfiguration> customize();
}
