package com.wujunshen.redis.properties;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Builder.Default;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.boot.context.properties.ConfigurationProperties;

/**
 * @author frank woo(吴峻申) <br>
 * email:<a href="mailto:frank_wjs@hotmail.com">frank_wjs@hotmail.com</a> <br>
 * @date 2020/2/7 5:33 下午 <br>
 */
@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
@ConfigurationProperties(prefix = "redis.pool")
public class RedisPoolProperties {
    /**
     * 最大空闲连接数
     */
    @Default
    private int maxIdle = 8;
    
    /**
     * 最小空闲连接数
     */
    @Default
    private int minIdle = 0;
    
    /**
     * 最大连接数
     */
    @Default
    private int maxActive = 8;
    
    /**
     * 当池内没有可用的连接时，最大等待时间
     */
    @Default
    private int maxWait = -1;
}
