package com.wujunshen.redis.properties;

import java.util.List;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Builder.Default;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.boot.context.properties.ConfigurationProperties;

/**
 * @author frank woo(吴峻申) <br>
 * email:<a href="mailto:frank_wjs@hotmail.com">frank_wjs@hotmail.com</a> <br>
 * @date 2020/2/7 5:33 下午 <br>
 */
@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
@ConfigurationProperties(prefix = "redis.cluster")
public class RedisProperties {
    /**
     * redis连接密码
     */
    private String password;
    
    /**
     * redis连接和执行的超时时间
     */
    private int timeOut;
    
    /**
     * 以逗号分隔的"host:port"redis集群节点信息
     */
    private List<String> nodes;
    
    /**
     * 获取失败 最大重定向次数
     */
    @Default
    private Integer maxRedirects = 6;
}
