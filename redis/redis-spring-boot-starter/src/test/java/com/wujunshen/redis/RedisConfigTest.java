package com.wujunshen.redis;

import static org.hamcrest.Matchers.equalTo;
import static org.junit.Assert.assertThat;

import com.wujunshen.redis.wrapper.MyRedisTemplate;
import javax.annotation.Resource;
import lombok.extern.slf4j.Slf4j;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.boot.test.context.SpringBootTest;

/**
 * @author <a href="mailto:frank_wjs@hotmail.com">吴峻申</a><br>
 * @date 2017/9/15 <br>
 */
@Slf4j
@SpringBootTest(classes = TestConfiguration.class)
public class RedisConfigTest {
    @Resource
    private MyRedisTemplate myRedisTemplate;
    
    @BeforeEach
    public void setUp() {
    }
    
    @AfterEach
    public void tearDown() {
    }
    
    @Test
    public void testSetStringValue() {
        myRedisTemplate.setExpire("xx", "111", 60 * 60 * 2L);
        
        assertThat(myRedisTemplate.getBy("xx"), equalTo("111"));
    }
}
