package com.wujunshen.redis.config;

import com.wujunshen.redis.custom.AbstractRedisCustomizer;
import com.wujunshen.redis.service.SampleCacheService;
import java.time.Duration;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;
import org.springframework.context.annotation.Configuration;
import org.springframework.data.redis.cache.RedisCacheConfiguration;

/**
 * @author frank woo(吴峻申) <br>
 * email:<a href="mailto:frank_wjs@hotmail.com">frank_wjs@hotmail.com</a> <br>
 * @date 2020/2/7 5:33 下午 <br>
 */
@Configuration
public class SampleRedisCustomizer extends AbstractRedisCustomizer {
    @Override
    public Map<String, RedisCacheConfiguration> customize() {
        Map<String, RedisCacheConfiguration> result = new ConcurrentHashMap<>(16);
        result.put(SampleCacheService.CACHE_NAME, buildCacheConfiguration(Duration.ofSeconds(10)));
        return result;
    }
}
