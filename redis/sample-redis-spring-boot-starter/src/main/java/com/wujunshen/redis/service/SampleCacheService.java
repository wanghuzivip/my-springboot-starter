package com.wujunshen.redis.service;

import com.wujunshen.redis.entity.Book;
import lombok.extern.slf4j.Slf4j;
import org.springframework.cache.annotation.CacheEvict;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.stereotype.Service;

/**
 * @author frank woo(吴峻申) <br>
 * email:<a href="mailto:frank_wjs@hotmail.com">frank_wjs@hotmail.com</a> <br>
 * @date 2020/2/7 5:33 下午 <br>
 */
@Slf4j
@Service
public class SampleCacheService {
    public static final String CACHE_NAME = "SAMPLE_BOOK";
    
    @Cacheable(value = CACHE_NAME)
    public Book getBookById(Integer id) {
        Book book = new Book();
        book.setBookId(id);
        book.setBookName("wujunshen");
        book.setPublisher("wujunshen");
        return book;
    }
    
    @CacheEvict(value = CACHE_NAME)
    public void deleteBookById(long id) {
    }
}
