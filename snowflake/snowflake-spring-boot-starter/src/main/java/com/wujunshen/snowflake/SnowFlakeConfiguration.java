package com.wujunshen.snowflake;

import com.wujunshen.snowflake.properties.Generate;
import com.wujunshen.snowflake.service.IdService;
import com.wujunshen.snowflake.service.impl.IdServiceImpl;
import javax.annotation.Resource;
import lombok.extern.slf4j.Slf4j;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

/**
 * @author frank woo(吴峻申) <br>
 * email:<a href="mailto:frank_wjs@hotmail.com">frank_wjs@hotmail.com</a> <br>
 * @date 2020/2/7 5:33 下午 <br>
 */
@Slf4j
@Configuration
@EnableConfigurationProperties({Generate.class})
public class SnowFlakeConfiguration {
    @Resource
    private Generate generate;
    
    @Bean(name = "idService")
    public IdService idService() {
        log.info("worker id is :{}", generate.getWorker());
        return new IdServiceImpl(Long.parseLong(generate.getWorker()));
    }
}
