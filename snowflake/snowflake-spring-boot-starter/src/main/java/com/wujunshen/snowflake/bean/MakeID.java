package com.wujunshen.snowflake.bean;

import com.fasterxml.jackson.annotation.JsonProperty;
import javax.validation.constraints.Max;
import javax.validation.constraints.Min;
import lombok.Data;

/**
 * @author frank woo(吴峻申) <br>
 * email:<a href="mailto:frank_wjs@hotmail.com">frank_wjs@hotmail.com</a> <br>
 * @date 2020/2/7 5:33 下午 <br>
 */
@Data
public class MakeID {
    @Max(1023)
    @Min(0)
    @JsonProperty("worker")
    private long machine = -1;
    
    @JsonProperty("timeStamp")
    private long time = -1;
    
    @Max(4095)
    @Min(0)
    @JsonProperty("sequence")
    private long seq = -1;
}
