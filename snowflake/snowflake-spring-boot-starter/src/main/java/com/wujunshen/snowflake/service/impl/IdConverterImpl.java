package com.wujunshen.snowflake.service.impl;

import com.wujunshen.snowflake.bean.ID;
import com.wujunshen.snowflake.bean.IdMeta;
import com.wujunshen.snowflake.service.IdConverter;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * @author frank woo(吴峻申) <br>
 * email:<a href="mailto:frank_wjs@hotmail.com">frank_wjs@hotmail.com</a> <br>
 * @date 2020/2/7 5:33 下午 <br>
 */
@Data
@NoArgsConstructor
public class IdConverterImpl implements IdConverter {
    @Override
    public long convert(ID id) {
        long result = 0;
        
        result |= id.getSequence();
        
        result |= id.getWorker() << IdMeta.SEQUENCE_BITS;
        
        result |= id.getTimeStamp() << IdMeta.TIMESTAMP_LEFT_SHIFT_BITS;
        
        return result;
    }
    
    @Override
    public ID convert(long id) {
        ID result = new ID();
        
        result.setSequence(id & IdMeta.SEQUENCE_MASK);
        
        result.setWorker((id >>> IdMeta.SEQUENCE_BITS) & IdMeta.ID_MASK);
        
        result.setTimeStamp((id >>> IdMeta.TIMESTAMP_LEFT_SHIFT_BITS) & IdMeta.TIMESTAMP_MASK);
        
        return result;
    }
}
