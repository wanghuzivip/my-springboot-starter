package com.wujunshen.snowflake.service.impl;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.core.IsEqual.equalTo;

import com.wujunshen.snowflake.bean.ID;
import com.wujunshen.snowflake.service.IdConverter;
import lombok.extern.slf4j.Slf4j;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

/**
 * @author frank woo(吴峻申) <br>
 * email:<a href="mailto:frank_wjs@hotmail.com">frank_wjs@hotmail.com</a> <br>
 * @date 2020/2/7 5:33 下午 <br>
 */
@Slf4j
public class IdConverterImplTest {
    private long id;
    
    @BeforeEach
    public void setUp() {
        id = 352608540609069079L;
    }
    
    @AfterEach
    public void tearDown() {
        id = 0L;
    }
    
    @Test
    public void convert() {
        IdConverter idConverter = new IdConverterImpl();
        
        ID actual = idConverter.convert(id);
        assertThat(actual.getSequence(), equalTo(23L));
        assertThat(actual.getWorker(), equalTo(92L));
        assertThat(actual.getTimeStamp(), equalTo(84068427231L));
        
        assertThat(idConverter.convert(actual), equalTo(id));
    }
}
