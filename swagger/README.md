# swagger

#### 项目介绍
参见swagger-spring-boot-starter项目

在/resources/META_INF/spring.factories文件中指定配置类SwaggerAutoConfiguration

然后在应用里加上swagger的相关配置就可访问swagger的web界面。

为了保证部署到生产环境之后的安全性问题，可通过swagger.enable=false关闭swagger访问权限

#### 使用说明
参见sample-swagger-spring-boot-starter项目

1. 在pom文件中加入starter依赖
        
    ```
    <dependency>
        <groupId>com.wujunshen</groupId>
        <artifactId>swagger-spring-boot-starter</artifactId>
        <version>0.0.1-SNAPSHOT</version>
    </dependency>
    ```

2. 在application.yml文件中加入下列配置

    ```
    #swagger config
    swagger:
      enable: true
      contextPath: /
      #输入目前已知所有版本号集合,即使重复了也可以,有去重功能
      versions: v1.0,v1.1,v1.2,v1.0
      #缺省配置内容为:frank_wjs@hotmail.com
      contact-email: frank_wjs@hotmail.com
      #缺省配置内容为:黑暗浪子
      contact-name: 如有任何疑问，请联系我
      #缺省配置内容为:https://www.iteye.com/blog/user/darkranger
      contact-url: http://www.google.com
      #缺省配置内容为:文档内容仅供参考
      description: 接口文档说明
      #缺省配置内容为:Apache License Version 2.0
      license: license
      #缺省配置内容为:http://www.apache.org/licenses/LICENSE-2.0.html
      license-url: http://www.license.com
      #缺省配置内容为:API文档
      title: 自定义标题名
    ```

3. 启动sample-swagger-spring-boot-starter应用后，运行[http://localhost:8012/swagger-ui.html](http://localhost:8012/swagger-ui.html)查看sample-swagger-spring-boot-starter应用是否能正常工作

    如下列这些图
![输入图片说明](https://images.gitee.com/uploads/images/2020/0322/214428_511eacc9_43183.png "屏幕截图.png")

![输入图片说明](https://images.gitee.com/uploads/images/2020/0322/214510_0400228a_43183.png "屏幕截图.png")

![输入图片说明](https://images.gitee.com/uploads/images/2020/0322/214540_94acc71c_43183.png "屏幕截图.png")

![输入图片说明](https://images.gitee.com/uploads/images/2020/0322/214632_ca09fad6_43183.png "屏幕截图.png")