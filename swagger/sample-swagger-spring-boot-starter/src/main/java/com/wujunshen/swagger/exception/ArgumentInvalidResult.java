package com.wujunshen.swagger.exception;

import lombok.Data;

/**
 * @author frank woo(吴峻申) <br>
 * email:<a href="mailto:frank_wjs@hotmail.com">frank_wjs@hotmail.com</a> <br>
 * @date 2020/2/7 5:33 下午 <br>
 */
@Data
public class ArgumentInvalidResult<T> {
    private String field;
    private T rejectedValue;
    private String defaultMessage;
}
