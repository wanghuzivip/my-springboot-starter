package com.wujunshen.swagger.exception;

import lombok.AllArgsConstructor;
import lombok.Getter;

/**
 * @author frank woo(吴峻申) <br>
 * email:<a href="mailto:frank_wjs@hotmail.com">frank_wjs@hotmail.com</a> <br>
 * @date 2020/2/7 5:33 下午 <br>
 */
@Getter
@AllArgsConstructor
public enum ResponseStatus {
    OK(200, "成功"),
    DATA_CREATE_ERROR(100, "新增数据失败"),
    DATA_RE_QUERY_ERROR(101, "查询数据失败"),
    DATA_UPDATED_ERROR(102, "更新数据失败"),
    DATA_DELETED_ERROR(103, "删除数据失败"),
    DATA_INPUT_ERROR(104, "数据未输入"),
    PARAMETER_VALIDATION(105, "参数验证失败-{0}"),
    PARAMETER_ERROR(106, "参数错误");
    
    /**
     * 成员变量
     */
    private int code;
    
    private String message;
}
