package com.wujunshen.swagger.service;

import com.wujunshen.swagger.dao.BookMapper;
import com.wujunshen.swagger.entity.Book;
import com.wujunshen.swagger.entity.BookCriteria;
import java.util.ArrayList;
import java.util.List;
import javax.annotation.Resource;
import org.springframework.stereotype.Service;

/**
 * @author frank woo(吴峻申) <br>
 * email:<a href="mailto:frank_wjs@hotmail.com">frank_wjs@hotmail.com</a> <br>
 * @date 2020/2/7 5:33 下午 <br>
 */
@Service
public class BookService {
    @Resource
    private BookMapper bookMapper;
    
    public int saveBook(final Book book) {
        return bookMapper.addSelective(book);
    }
    
    public List<Book> getBooks() {
        return new ArrayList<>(bookMapper.selectByExample(new BookCriteria()));
    }
    
    public int updateBook(Integer bookId, Book book) {
        book.setBookId(bookId);
        BookCriteria bookCriteria = new BookCriteria();
        BookCriteria.Criteria criteria = bookCriteria.createCriteria();
        criteria.andBookIdEqualTo(bookId);
        return bookMapper.updateByExample(book, bookCriteria);
    }
    
    public int deleteBook(Integer bookId) {
        return bookMapper.deleteByPrimaryKey(bookId);
    }
    
    public Book getBook(Integer bookId) {
        return bookMapper.selectByPrimaryKey(bookId);
    }
}
